<section>

<?php echo validation_errors(); ?>

<?php echo form_open('contact'); 

	$username = array(
							'name' => 'username',
							'placeholder' => 'Username',
							'id' => 'username',
							'type' => 'text'
							);
							
	echo form_label('username');						
	echo form_input($username);	

	$password = array(
								'name' => 'password', 
								'placeholder' => 'Password',
								'id' 	   => 'password',
								);
								
	echo form_label('password');
	echo form_password($password);
	
	$password_confirm = array(
								'name' => 'password_confirm', 
								'placeholder' => 'Password Confirm',
								'id' 	   => 'password_confirm',
							);
	
	echo form_label("confirm password");
	echo form_password('$password_confirm');
	
	$email = array(
							'name' => 'email',
							'placeholder' => 'Email',
							'id' => 'email',
							'type' => 'email'
						);
	
	
	echo form_label('email');
	echo form_input($email);
?>

<div><input type="submit" value="Submit" /></div>

<?php 
	

	
	echo form_close(); 

?>


</section>