<?php echo doctype('html5'); ?>
<html>
<head>
	<title><?php echo $title; ?></title>
	<?php 
		echo meta('description', $metaDescription);
		echo link_tag('css/main.css'); 
	?>

</head>

<body>

<div id="main">

<div id="heading">

<div id="title"><?php echo heading("CodeIgniter Template", 1); ?></div>

<!-- main nav -->
<nav>
	<li><a href="<?php echo base_url(); ?>index">Home</a></li>
	<li><a href="<?php echo base_url(); ?>about">About</a></li>
	<li><a href="<?php echo base_url(); ?>contact">Contact</a></li>
	<li><a href="<?php echo base_url(); ?>gallery">Gallery</a></li>
	<li><a href="<?php echo base_url(); ?>location">Location</a></li>
</nav>

<?php echo heading($heading, 2); ?>
					
</div>
						
	<?php 
	
	//Associtive array to dispaly image and it's properties
	$image_properties = array(
			
			'src' => 'img/Codeigniter.png',
			'alt' => 'Codeigniter',
			'class' => 'myClass',
	
	);
	
	?>
	
<div id="logo-img"><?php	echo img($image_properties); ?></div>
	
	