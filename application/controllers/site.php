<?php

class Site extends CI_Controller{
	
	function index()
	{
				// Associative Array to display page properties
			$data = array(
				'title' => 'Home',
				'content' => 'home',
				'heading' => 'Home',
				'metaDescription' => 'CI Site',
			);		
			
			$this->load->view("template", $data);	
			
	}

	
	function about()
	{
				// Associative Array to display page properties		
			$data = array(
				'title' => 'About',
				'content' => 'about',
				'heading' => 'About',
				'metaDescription' => 'CI Site',
			);	
		
			$this->load->view("template", $data);
		
	}
	
	function contact()
	{
				// Associative Array to display page properties			
			$data = array(
				'title' => 'Contact',
				'content' => 'contact',
				'heading' => 'Contact',
				'metaDescription' => 'CI Site',
			);	
			
		$this->form_validation->set_rules('username', 'Username', 'required');		
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
					
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view("template", $data);
		}
		else
		{
			$this->load->view('includes/formsuccess');
		}				
		
	}
	
	function location()
	{
				// Associative Array to display page properties
			$data = array(
				'title' => 'Location',
				'content' => 'location',
				'heading' => 'Location',
				'metaDescription' => 'CI Site',
			);	
		
			$this->load->view("template", $data);
	}
	
	function gallery()
	{
		$this->load->view('gallery');
	}
}